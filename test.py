#-*- coding:utf-8 -*-
from hacked_urllib2 import urllib2
from cookielib import CookieJar

def test(url):
    cj = CookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
    response = opener.open(url)
    
    with open('test.html', 'wt') as f:
        f.write(response.read())
    
    import webbrowser
    webbrowser.open("test.html")

if __name__ == "__main__":
    url = ""  # <---- url 변수에가다 차단된 주소를 넣어보세요. 
    test(url)
    